
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from plotly.subplots import make_subplots

from statsmodels.tsa.seasonal import seasonal_decompose # should really be called from eda

class CollaboratePreliminaryAnalysis():
    
    def get_percentage_change_cols(self, df, cols_parent = None):
        """Takes a list of data frame columns and plots the percetage change"""
        if not cols_parent:
            cols_parent = list(df)
            print(cols_parent)
        else:
            cols_parent = cols_parent 
        for i in cols_parent:
            df[f"percentage_change_{i}"] = df[i].pct_change()
        return df 
    
    


    
    
    def plot_telemetry_df(self, df, time_col, title = None):
        """Plots a telemenetry based dataFrame"""
        fig = px.line(df, 
                      x=time_col, 
                      y=df.columns,
                      title=title)
        fig.update_xaxes(
        dtick="M1",
        tickformat="%b\n%Y")
        return fig
    
    def get_scatter_plot(self, df, x , y):
        fig = px.scatter(x=df[x], y=[y])
        return fig
    
    def get_timeseries_with_slider(self, df,  title = "title", date_col = 'measurement_timestamp', feature = 'measurement_value'):
        """Get a time series witrh slider for fetare of dataframe df with date column date_col"""
        fig = px.line(df,
                  x=date_col, 
                  y= feature, 
                  title=f'title: {feature}')

        fig.update_xaxes(
            rangeslider_visible=True,
            rangeselector=dict(
                buttons=list([
                    dict(count=1, label="1m", step="month", stepmode="backward"),
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="YTD", step="year", stepmode="todate"),
                    dict(count=1, label="1y", step="year", stepmode="backward"),
                    dict(step="all")
                ])
            )
        )

        return fig
    
    def get_xaxis_title(self, column_header):
        """gets the title for x axis"""
        if "debiet" in column_header:
            return "Flow Rate (m3/hr)"
        elif "Chloor" in column_header:
            return "Chlorine (mg/L)"
        elif "pH" in column_header:
            return "pH"
        else:
            return "Axis label couldnt be infered from column header"
    

    def get_stacked_scatter(self, df, xcol, exclude_cols = None):
        """gets a stacked scatter for df"""
        if exclude_cols:
            df = df[[i for i in list(df) if i not in exclude_cols]]
        else:
            df = df 
        nrows = len(list(df)) - 1

        print("n rows ", nrows)

        subplot_columns = tuple([i for i in list(df) if i not in [xcol]])

        fig = make_subplots(rows= nrows,
                            cols=1, 
                            shared_xaxes=True,
                            x_title= 'Date',
                            subplot_titles= subplot_columns )

        x = df[xcol]
        row = 0
        for i in subplot_columns:
            #print(i)
            row += 1
            fig.append_trace(go.Scatter(
                x= x, # datetime 
                y= df[i].values,  #dfmeasures['rainfall'].values,
                name = i, 

                ), row= row , col=1)
            fig.update_yaxes(title_text= self.get_xaxis_title(column_header = i), row=row, col=1)

        fig.update_layout(
            #xaxis = {'rangeslider' : {'visible' : True}}
        )

        #fig['layout']['sliders'][0]['pad']=dict(r= 10, t= 150,)

        return fig 
    
    def get_boxplots(self, df, jitter = 0.3, pointposition = -1.8, boxpoints = 'all', boxmean = 'sd'):

        """For a site gets the hsitograms for each feature """

        fig = make_subplots(rows=3, 
                            cols=3, 
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1',
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                           )


        
        row = 1 
        col = 1 
        colcount = 1
        
        for i in list(df):
            fig.add_trace(go.Box(y=df[i], 
                                boxmean=boxmean, 
                                name= i, 
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition,
                                    ),
                                    row=row, 
                                    col=col, 
                                 )
            
                       
            if colcount == 3:
                row += 1
                col = 0
                colcount = 0
                
            col += 1
            colcount += 1

       
        return fig

    def get_histograms(self, df, histnorm='probability'):

        """For a site gets the hsitograms for each feature """

        fig = make_subplots(rows=3, 
                            cols=3, 
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1',
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                            )
        
        row = 1 
        col = 1 
        colcount = 1
        
        for i in list(df):
            fig.add_trace(go.Histogram(
                                    x=df[i],
                                    name= i,
                                    histnorm= histnorm,
                                    ),
                                    row=row, 
                                    col=col, 
                                 )
            
                       
            if colcount == 3:
                row += 1
                col = 0
                colcount = 0
                
            col += 1
            colcount += 1


      
        return fig
    
    
    
    def get_sactterplots(self, df):

        """For a site gets the hsitograms for each feature """
        
        # Need to make this so that it works for all sites out the box no matter how many item s
        cols = 3
        rows = 3
        fig = make_subplots(rows=rows, # need to be det by the number to plot
                            cols=cols, # need to be det by the number to plot this is for 9 i.e 3*3
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1', #from list df but how does this work when cols excluded? 
                                                          # cahnge elsewhere i.e harmonise compraables before they get here 
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                            )
        
        
        row = 1 
        col = 1 
        colcount = 1
        
        for i in list(df):
            #print("row ", row)
            #print("col ", col)
            #print("col count " , colcount)
            fig.add_trace(go.Scatter(
                                    x=df.index, 
                                    y=df[i], 
                                    name= i,
                                    ),
                                    row=row, 
                                    col=col, 
                                 )
            
                       
            if colcount == 3:
                row += 1
                col = 0
                colcount = 0
                
            col += 1
            colcount += 1
                
        
            
        
        
        
        return fig
    
    
    def get_eda_ts_plots(self, 
                         df, 
                         feature, 
                         site = None, 
                         histnorm='probability', 
                         jitter = 0.3, 
                         pointposition = -1.8, 
                         boxpoints = 'all'):

            """For a site gets the hsitograms for each feature """

            fig = make_subplots(rows=6, 
                                cols=2, 
                                start_cell="top-left", 
                                specs=[[{"colspan": 2}, None], 
                                      [{"colspan": 2}, None], 
                                      [{"colspan": 2}, None], 
                                      [{"colspan": 2}, None],
                                      [{"rowspan": 2}, {"rowspan": 2}],
                                      [None, None], 
                                     
                                      ],
                                subplot_titles=('Time Series',
                                                 'Trend - seasonal decomposition', 
                                                 'Seasonal - seasonal decomposition', 
                                                 'Residuals - seasonal decomposition', 
                                                 'Boxplot',
                                                 'Histogram',
                                                 'Outliers',
                                                 #'Maybe another',
                                                ), 
                                #vertical_spacing = 0.25, 
                                row_heights = [1000 for i in range(0, 6)], 
                                )

            fig.add_trace(go.Scatter(x=list(df.index), 
                                     y=list(df[feature]), 
                                     name = feature
                                    ), 
                              row=1, 
                              col=1,
                         )


            decomposition = self.get_seasonal_decomposition_sm(df[feature])



            fig.add_trace(go.Scatter(x= decomposition.trend.index, #list(decomposition.seasonal.index), 
                                 y=list(decomposition.trend),

                                 #x= x, #list(decomposition.trend.index), 
                                 #y=list(decomposition.trend), 
                                 name = 'trend'
                                ), 
                          row=2, 
                          col=1,
                     )

            fig.add_trace(go.Scatter(x= decomposition.seasonal.index, #list(decomposition.seasonal.index), 
                                 y=list(decomposition.seasonal), 
                                 name = 'seasonal'
                                ), 
                          row=3, 
                          col=1,
                     )

            fig.add_trace(go.Scatter(x=list(decomposition.resid.index), 
                                 y=list(decomposition.resid), 
                                 name = 'residuals'
                                ), 
                          row=4, 
                          col=1,
                     )

            fig.add_trace(go.Box( 
                                    y=df[feature], 
                                    boxmean='sd',
                                    name= 'boxplot',
                                    boxpoints=boxpoints, 
                                    jitter=jitter, 
                                    pointpos=pointposition, 
                                    ),
                                    row=5, 
                                    col=1, 
                                 )


            fig.add_trace(go.Histogram( 
                                    x=df[feature], 
                                    histnorm= histnorm, 
                                    name = 'histogram',
                                    ),
                                    row=5, 
                                    col=2, 
                                 )
            
            
            
            title = lambda feature, site: f"EDA plots {feature} - {site}" if site else f"EDA plots {feature}"

            fig.layout.update(title= title(feature, site), 
                              height=1200, 
                              width=900, 
                              showlegend=False, 
                              hovermode='closest', 
                             )






            return fig
        
        # EDA TS seasonal decomposition 

    def get_seasonal_decomposition_sm(self, series):
        decomposition = seasonal_decompose(series.fillna(0), model='additive')
        return decomposition


    def seasonal_decomposition(self, series, showts = False):

        decomposition = self.get_seasonal_decomposition_sm(series)


        """For a site gets the hsitograms for each feature """
        if not showts:
            x = series.index
            fig = make_subplots(rows=3, 
                            cols=1, 
                            start_cell="top-left",  
                            #x_title = 'Date', 
                            subplot_titles=('seaonal',
                                            'trend',
                                             'residuals',
                                            ), 

                            )

            fig.add_trace(go.Scatter(x= x, #list(decomposition.seasonal.index), 
                                 y=list(decomposition.seasonal), 
                                 name = 'seasonal'
                                ), 
                          row=1, 
                          col=1,
                     )

            fig.add_trace(go.Scatter(x= decomposition.trend.index, #list(decomposition.seasonal.index), 
                                 y=list(decomposition.trend),

                                 #x= x, #list(decomposition.trend.index), 
                                 #y=list(decomposition.trend), 
                                 name = 'trend'
                                ), 
                          row=2, 
                          col=1,
                     )

            fig.add_trace(go.Scatter(x=list(decomposition.resid.index), 
                                 y=list(decomposition.resid), 
                                 name = 'residuals'
                                ), 
                          row=3, 
                          col=1,
                     )
        else:
             fig = make_subplots(rows=1, 
                            cols=4, 
                            start_cell="top-left", 
                            subplot_titles=('series'
                                            'seaonal',
                                            'trend',
                                             'residuals',
                                            ), 
                            )


        fig.update_layout(  title="Seasonal Decomposition",
                            #xaxis_title="X Axis Title",
                            #yaxis_title="Y Axis Title",
                            legend_title="Legend Title",
                            font=dict(
                                family="Lato",
                                size=10,
                                #color="RebeccaPurple"
                            )
                        )

        return fig




