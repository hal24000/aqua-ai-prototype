import plotly 

class ColloaborateHtmlTemplates():
    
    def get_address_bar(self, logo_path = "collaborate-dimension.png" ):
        
        """Creates an add bar for html reports"""
        
            
        collaboarte_width = 2527 
        collaboarte_height = 384
        collaboarte_ratio_height_to_width = collaboarte_width/collaboarte_height

        logo_height = 500

        add_bar = f"""  
            <img src={logo_path} 
            width=f"{logo_height}px" 
            height="{logo_height/collaboarte_ratio_height_to_width}px" >
            <br>
            <div class="topnav", 
                            style = "font-size:32px;background-color: rgb(255, 102, 0);margin-top:10px">

              <a class="active" href="index.html">Overview</a> 
              <a > &nbsp; Sites: &nbsp; </a>
              <a href="assende.html"
              style = "margin-right:3px" style = "margin-right:10px" > Assenede </a>
              <a href="eekloo.html" style = "margin-right:3px" > Eekloo </a>
              <a href="kluizen.html"> Kluizen </a>

            </div>  

            <hr>"""



        return add_bar
    
    def get_address_bar_by_features(self, SITE, RESAMPLE_TIME, logo_path = "../static/images/collaborate-dimension.png" ):
        
        """Creates an add bar for html reports"""
        
            
        collaboarte_width = 2527 
        collaboarte_height = 384
        collaboarte_ratio_height_to_width = collaboarte_width/collaboarte_height

        logo_height = 500
        

        add_bar = f"""  
            <div  style = "position:-webkit-sticky; position:sticky;">
            <img src={logo_path} 
            width=f"{logo_height}px" 
            height="{logo_height/collaboarte_ratio_height_to_width}px" >
            <br>
            <div class="topnav", 
                            style = "font-size:32px;background-color: rgb(255, 102, 0);margin-top:10px">

              <a class="active" href="index-{SITE}-Resampled-at-{RESAMPLE_TIME}.html">Overview</a> 
              <a > &nbsp; Features: &nbsp; </a>
              <a href="DetectorB1-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"
              style = "margin-right:3px" style = "margin-right:10px" > DetectorB1 </a>
              <a href="DetectorB2-{SITE}-Resampled-at-{RESAMPLE_TIME}.html" style = "margin-right:3px" > DetectorB2 </a>
              <a href="DetectorB3-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> DetectorB3 </a>
              <a href="temperature-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> Temprature </a>
              <a href="Response-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> Response </a>
              <a href="SignalHealth-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> SignalHealth </a>
              <a href="F24Response-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> F24Response </a>
              <a href="Threshold1-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> Threshold1 </a>
              <a href="Threshold2-{SITE}-Resampled-at-{RESAMPLE_TIME}.html"> Threshold2 </a>
              

            </div>  

            <hr>
            </div>"""



        return add_bar
    
    def get_plotly_html(self, fig):
        return plotly.io.to_html(fig)

    def save_html_file(self, 
                       html_str = "<H1> YTesting html write</H1>",
                       filename = "test.html"):

        with open(filename,"w") as f:
            f.write(html_str)

        return (f"saved file {filename}")


