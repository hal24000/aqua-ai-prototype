import os
import glob
import pandas as pd

class CollaborateManageFiles():
    
    def get_files_in_folder(self, PATH = os.getcwd(),  recursive = True, ext_ = 'csv' ):
        """Lists files in folder by extension"""
        if recursive:
            result = [y for x in os.walk(PATH) for y in glob.glob(os.path.join(x[0], f'*{ext_ }'))]
            result = [i.replace("\\","//") for i in result]
        else:
            pass
        return result 
        
    def get_files_by_extension(self, directory, file_extension ):
        """gets the files in a directory by extension"""
        file = []
        file_dict = {}
        
        for file in os.listdir(directory ):
            if file.endswith(file_extension):
                #print(os.path.join(directory,  file))
                if "assenede" in file:
                    if "assenede" in file_dict:
                        # append the new number to the existing array at this slot
                        file_dict["assenede"].append(os.path.join(directory,  file))
                    else:
                        # create a new array in this slot
                        file_dict["assenede"] = [os.path.join(directory,  file)]
                if "eeklo" in file:
                    if "eeklo" in file_dict:
                        # append the new number to the existing array at this slot
                        file_dict["eeklo"].append(os.path.join(directory,  file))
                    else:
                        # create a new array in this slot
                        file_dict["eeklo"] = [os.path.join(directory,  file)]
                if "kluizen" in file:
                    if "kluizen" in file_dict:
                        # append the new number to the existing array at this slot
                        file_dict["kluizen"].append(os.path.join(directory,  file))
                    else:
                        # create a new array in this slot
                        file_dict["kluizen"] = [os.path.join(directory,  file)]
                        
        
        return file_dict
    
    def combine_files_to_dataframe(self, file_list, sort_col = None ):
        """"""
        df = pd.DataFrame()
        for v in file_list:
            print(v)
            df_ = pd.read_csv(v, 
                        delimiter = ";", 
                        decimal=',')
            
            df = df.append(df_)
            
        if sort_col:
            df = df.sort_values(sort_col)
        else:
            print("no sort column")
            
        df = df.drop_duplicates() # bug
            
        return df
