collaborate package
===================

Submodules
----------

collaborate.exploratory\_data\_analysis module
----------------------------------------------

.. automodule:: collaborate.exploratory_data_analysis
   :members:
   :undoc-members:
   :show-inheritance:

collaborate.file\_management module
-----------------------------------

.. automodule:: collaborate.file_management
   :members:
   :undoc-members:
   :show-inheritance:

collaborate.html\_helpers module
--------------------------------

.. automodule:: collaborate.html_helpers
   :members:
   :undoc-members:
   :show-inheritance:

collaborate.preliminary\_data\_analysis module
----------------------------------------------

.. automodule:: collaborate.preliminary_data_analysis
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: collaborate
   :members:
   :undoc-members:
   :show-inheritance:
