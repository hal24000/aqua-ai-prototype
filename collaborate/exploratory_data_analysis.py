import scipy

import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler
from statsmodels.tsa.stattools import adfuller, kpss
#from statsmodels.tsa.seasonal import seasonal_decompose

class CollaboarteEDA():
    """A class holding methods for EDA on dataframe defined at instantation"""

    #@property
    #def _constructor(self):
    #    return SubclassedDataFrame


    def __init__(self, df):
        self.df = df
    
    

    def get_inter_quartile_range_data(self, upperq = 0.75, lowerq = 0.25):
        
        dict_iqrs = {}
        lower_outliers = {}
        lower_outliers_count = {}
        lower_outliers_per = {}
        upper_outliers = {}
        upper_outliers_count = {}
        upper_outliers_per = {}
        df_upper_outliers = pd.DataFrame()
        df_lower_outliers = pd.DataFrame()
        
        for col in list(self.df):
            
            if self.df[col].dtype != np.dtype('object'):
                #print(f"column {col} is not an object calculating IQR")
                q1 = self.df[col].quantile(lowerq)
                #print(f"q1 {col} is {q1}")
                q3 = self.df[col].quantile(upperq)
                #print(f"q3 {col} is {q3}")
                iqr = q3 - q1
                dict_iqrs[col] = iqr
                
                lower_outlier_values = self.df[col].loc[self.df[col] < (q1 - (iqr *1.5)) ].values
                lower_outliers[col] = lower_outlier_values
                lower_outliers_count[col] = len(lower_outlier_values)
                lower_outliers_per[col] = (len(lower_outlier_values)/len(self.df))*100
                
                upper_outlier_values = self.df[col].loc[self.df[col] > (q3 + (iqr *1.5))].values
                upper_outliers[col] = upper_outlier_values
                upper_outliers_count[col] = len(upper_outlier_values)
                upper_outliers_per[col] = (len(upper_outlier_values)/len(self.df))*100
                
                df_upper_outliers_ = self.df.loc[self.df[col] > (q3 + (iqr *1.5))].copy()
                df_upper_outliers_[f"upper_feature_{col}"] = col
                df_upper_outliers = df_upper_outliers.append(df_upper_outliers_)
                
                df_lower_outliers_ = self.df.loc[self.df[col] < (q1 - (iqr *1.5)) ].copy()
                df_lower_outliers_[f"lower_feature_{col}"] = col
                df_lower_outliers = df_lower_outliers.append(df_lower_outliers_)
                
            else:
                print(col, "is an object skiped")
                
        df_result = pd.DataFrame.from_dict(dict_iqrs, orient = 'index').T
        df_result.rename({0: "IQR"}, inplace = True)
        
        df_lower_outiler_count = pd.DataFrame.from_dict(lower_outliers_count, orient = 'index').T
        df_lower_outiler_count.rename({0: "Suspected Lower Outlier Count"}, inplace = True)
        df_lower_outiler_per = pd.DataFrame.from_dict(lower_outliers_per, orient = 'index').T
        df_lower_outiler_per.rename({0: "Suspected Lower Outlier %"}, inplace = True)
        
        df_upper_outiler_count = pd.DataFrame.from_dict(upper_outliers_count, orient = 'index').T
        df_upper_outiler_count.rename({0: "Suspected Upper Outlier Count"}, inplace = True)
        df_upper_outiler_per = pd.DataFrame.from_dict(upper_outliers_per, orient = 'index').T
        df_upper_outiler_per.rename({0: "Suspected Upper Outlier %"}, inplace = True)
        
        df_result = df_result.append(df_lower_outiler_count)
        df_result = df_result.append(df_lower_outiler_per)
        
        df_result = df_result.append(df_upper_outiler_count)
        df_result = df_result.append(df_upper_outiler_per)
        
    
        return df_result, df_lower_outliers, df_upper_outliers  #, df_lower_outiler_count, upper_outliers_count

    def get_box_plot_values(self, column, unit):
        """for a given dataframe column (or subset of column) get the boxplot values for EDA
        could be used to test new fucntion 

        """
        df_ = self.df[column]


        count = df_.count()
        mean = df_.mean()
        std_dev = df_.std()

        variance = df_.var()

        mode = df_.mode()
        mode_count = len(df_.loc[ df_ == df_.mode().values[0]])
        median = df_.median()

        dict_general_stats = {'count' : count, 
                             'mean' : mean, 
                             'std_dev' : std_dev, 
                            'mode' : mode, 
                              'mode_count' : mode_count, 
                              'median' : median, 
                              'unit' : unit, 

                             }

        bp_data = pd.DataFrame.boxplot(df_,  return_type='dict')
        outliers = [flier.get_ydata() for flier in bp_data["fliers"]]
        boxes = [box.get_ydata() for box in bp_data["boxes"]]
        medians = [median.get_ydata() for median in bp_data["medians"]]
        whiskers = [whiskers.get_ydata() for whiskers in bp_data["whiskers"]]

        q0 = whiskers[0][1]
        q1 = whiskers[0][0]
        q3 = whiskers[1][0]
        q4 = whiskers[1][1]

        # get data on outliers 
        lower_outliers = [i for i in outliers[0] if i < q1]
        upper_outliers = [i for i in outliers[0] if i > q3]
        
        total_ouliers = len(lower_outliers) + len(upper_outliers)

        dict_outliers = { 'total outliers' :  total_ouliers, 
                        'outliers_percent' : total_ouliers/count *100, 

                        'lower_outliers' :  {'values' :lower_outliers, 
                         'num_lower_outliers' : len(lower_outliers), 
                        'min_lower_outlier' : min(lower_outliers, default=0), 
                        'max_lower_outlier' : max(lower_outliers, default=0), 
                                              }, 

                        'upper_outliers' :  {'values' : upper_outliers, 
                        'num_upper_outliers' : len(upper_outliers), 
                        'min_upper_outlier' : min(upper_outliers, default=0), 
                        'max_upper_outlier' : max(upper_outliers, default=0), 
                                              }
                           }

        # get median 
        # do we need to check medians are equal why are their 2 medians? 
        if medians[0][0] == medians[0][1]:
            print("matching of medians retunred")
        else:
            print ("mediand not matching, invetsigate: ", medians)

        median = medians[0][0]

        dict_quartiles = { 'q0' : q0, 
                          'q1' : q1, 
                          'median' : median, 
                          'q3' : q3, 
                          'q4' : q4, 
                          'IQR' : q3 - q1
                         }

        dict_boxplot_values = { 'dict_general_stats' : dict_general_stats, 
                                'dict_outliers' : dict_outliers, 
                               'dict_quartiles' : dict_quartiles

                              }
        return dict_boxplot_values 
    

    def get_enriched_df_describe(self ):
        """Enriches pandas describe with additional stat measure built into pandas"""
        original_cols = list(self.df)
        self.df = self.df.select_dtypes(include=np.number)
        numeric_cols = list(self.df)
        print(f"object columns dropeed: {list(set(original_cols) ^ set(numeric_cols))}")
        df_enriched = self.df.describe().append(pd.DataFrame(data = [
                         [i for i in self.df.median() ], 
                         [i for i in self.df.mode().values.tolist()[0]  if not isinstance(i, str)], 
                         [i for i in self.df.var()],
                         [i for i in self.df.kurtosis()], 
                         [i for i in self.df.skew()], 
                        ], 
                 index = ["median", "mode", "variance" , "kurtosis", "skew"], 
                 columns = [i for i in list(self.df) if i not in ["Sensor", "site"]]
                ))
        return df_enriched
    
    def get_full_stats_dataframe(self):
        """gets a stats dataframe using methods in the class"""
        df_stats = self.get_enriched_df_describe()
        df_iqr, df_lower_outliers, df_upper_outliers = self.get_inter_quartile_range_data()
        df_stats = df_stats.append(df_iqr)
        return df_stats, df_lower_outliers, df_upper_outliers
    
        # Stationary Checks  EDA 
    def simple_stationary_check(self, series, split = 2):
        """Simple test of stationarity"""
        X = series.values
        splitlen= round(len(X) / split)
        if split == 2:
            X1, X2 = X[0:splitlen], X[splitlen:]
            mean1, mean2 = X1.mean(), X2.mean()
            var1, var2 = X1.var(), X2.var()
            print('mean1=%f, mean2=%f' % (mean1, mean2))
            print('variance1=%f, variance2=%f' % (var1, var2))
            return 'foo'
        elif split == 4:
            X1, X2, X3, X4 = X[0:splitlen], X[splitlen:2*splitlen],  X[2*splitlen: 3*splitlen], X[ 3*splitlen:]
            mean1, mean2, mean3, mean4 = X1.mean(), X2.mean(), X3.mean(), X4.mean()
            var1, var2, var3, var4 = X1.var(), X2.var(), X3.var(), X4.var()
            print(f'mean1= {mean1}, mean2= {mean2}, mean3={mean3}, mean4={mean4} ')
            print(f'variance1= {var1}, variance2={var2}, variance3= {var3}, variance4={var4}')
            return 'foo'
        elif split == 6:
            X1, X2, X3, X4, X5, X6 = X[0:splitlen], X[splitlen:2*splitlen],  X[2*splitlen: 3*splitlen],\
                                     X[ 3*splitlen:4*splitlen], X[ 4*splitlen:5*splitlen], X[ 5*splitlen:]
            mean1, mean2, mean3, mean4, mean5, mean6 = X1.mean(), X2.mean(), X3.mean(), X4.mean(), X5.mean(), X6.mean()
            var1, var2, var3, var4, var5, var6 = X1.var(), X2.var(), X3.var(), X4.var(), X5.var(), X6.var()
            print(f'mean1= {mean1}, mean2= {mean2}, mean3={mean3}, mean4={mean4}, mean5={mean5}, mean6={mean6} ')
            print(f'variance1= {var1}, variance2={var2}, variance3= {var3}, variance4={var4}, variance5= {var5}, variance6={var6}')
            return 'foo'
        else:
            print("splitlen not suppooured")

    def adf_test(self, timeseries, feature = None):
        """feature only used for the name of dataframe column"""
        print("Results of Dickey-Fuller Test:")

        test_result = adfuller(timeseries, autolag="AIC")

        name = lambda feature: feature + " ADF" if feature else 'ADF'
        
        df_output = pd.Series(
            test_result[0:4],
            index=[
                "Test Statistic",
                "p-value",
                "#Lags Used",
                "Number of Observations Used",
            ],
            name = name(feature)
           
        )
        for key, value in test_result[4].items():
            df_output["Critical Value (%s)" % key] = value
        #print(df_output)
        if df_output["p-value"] <= 0.05:
            print("Null hypothesis rejected series in stationary")
            df_output.loc["stationary"]  = True
        elif df_output["p-value"] > 0.05:
            print("Null hypothesis not rejected series in non stationary")
            df_output.loc["stationary"]  = False 
        else:
            print("some error")
        return df_output.to_frame()

    def kpss_test(self, timeseries, feature = None, regression = "c", nlags = "auto"):
        """kpss test for testing of time series stationarity around a deterministic trend.
            feature only used for the name of dataframe column
        
        """
        if regression == "c":
            regression_type = "mean"
        elif regression == "ct":
            regression_type = "trend"
        else:
            return print("Regressor not supported")
        print("Results of KPSS Test:")

        test_result = kpss(timeseries, regression= regression, nlags= nlags)
        name = lambda feature, regression_type: feature + " KPSS" + regression_type if feature else "KPSS" + regression_type


        df_output = pd.Series(
            test_result[0:3],
            index=[
                "Test Statistic",
                "p-value",
                "#Lags Used",
                #"Number of Observations Used",
            ],
            name = name(feature, regression_type)
        
        )

        for key, value in test_result[3].items():
            df_output["Critical Value (%s)" % key] = value
        #print(df_output)
        if df_output["p-value"] <= 0.05:
            print(f"Null hypothesis rejected series in NON stationary about the {regression_type}")
            df_output.loc["stationary"]  = False
        elif df_output["p-value"] > 0.05:
            print(f"Null hypothesis not rejected series is stationary about the {regression_type}")
            df_output.loc["stationary"]  = True
        else:
            print("some error")
        return df_output.to_frame()

    def create_stationary_test_df(self, series, feature = None):
        
        df_adf = self.adf_test(series, feature = feature)
        try:# added as wont excpet a avlue all 1.0 or even 1.1 
            # As below calls to fucn 
            # data = [1.0 for i in range(0, 1000) ]
            # data.append(50) # works when we add a number 
            # create_stationary_test_df(data)

            df_kpss_mean = self.kpss_test(series, feature = feature, regression = "c")
            df_kpss_trend = self.kpss_test(series, feature = feature, regression = "ct")


        except Exception as e:
            print(e)
            df_kpss_mean = pd.DataFrame()
            df_kpss_trend = pd.DataFrame()

        df_stationary_tests = df_adf.join([df_kpss_mean, df_kpss_trend])
        return df_stationary_tests
    
    

